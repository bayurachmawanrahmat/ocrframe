//
//  OCRViewController.swift
//  OCRFrame
//
//  Created by Ubayz on 04/07/24.
//

import UIKit

class OCRViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var ambilGambar: UIButton!

    let imagePicker = UIImagePickerController()
    let frame = Card()
    let common = Common()
    var frameSize: CGSize = CGSize(width: 325.0, height: 230)
    var alert_title:String = ""
    var alert_message:String = ""
    var base64:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkPermision()
    }
    
    func checkPermision(){
        common.checkCameraPermissions { granted in
            if !granted {
                self.common.showAlert(view:self, title:self.alert_title, message:"Camera Access Denied")
            }else{
                self.ambilGambar.isEnabled = true
            }
        }
    }
    
    @IBAction func ambilGambar(_ sender: Any) {
        self.presentImagePickerController()
    }
    
    func presentImagePickerController() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            frame.create(frameSize: frameSize, picker: imagePicker, borderWidth: 2, color: UIColor.systemBlue.cgColor)
            imagePicker.cameraViewTransform = CGAffineTransformMakeTranslation(0.0, 50.0);
            present(imagePicker, animated: true, completion: nil)
        } else {
            common.showAlert(view:self, title:alert_title, message:alert_message)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        guard let frameImage = frame.crop(picker: picker) else {
            print("Failed to crop and flip image.")
            return
        }
        
        let newSize = CGSize(width: 450, height: 350)
        DispatchQueue.main.async {
            self.imageView.image = self.common.resizeImage(image: frameImage, targetSize: newSize)
        }
        base64 = self.imageView.image?.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }

}
