//
//  Card.swift
//  OCRFrame
//
//  Created by Ubayz on 04/07/24.
//

import UIKit

class Card: UIView {

    var outerFrameView = UIView()
        var innerFrameView = UIView()
        
        public func create(frameSize:CGSize, picker:UIImagePickerController, borderWidth:CGFloat, color:CGColor){
            let fSetup = Common()
            
            let outerFrameX = (picker.view.bounds.width - frameSize.width) / 2
            let outerFrameY = (picker.view.bounds.height - frameSize.height) / 2

            outerFrameView = UIView(frame: CGRect(x: outerFrameX, y: outerFrameY, width: frameSize.width, height: frameSize.height))
            fSetup.frameSetup(view:outerFrameView, borderWidth:borderWidth, color:color )
            
            let innerFrameX = (outerFrameView.bounds.width - (frameSize.width-25)) / 2
            let innerFrameY = (outerFrameView.bounds.height - (frameSize.height-30)) / 2

            innerFrameView = UIView(frame: CGRect(x: innerFrameX, y: innerFrameY, width: (frameSize.width-25), height: (frameSize.height-30)))
            fSetup.frameSetup(view:innerFrameView, borderWidth:borderWidth, color:UIColor.clear.cgColor )
            
            outerFrameView.addSubview(innerFrameView)
            picker.cameraOverlayView = outerFrameView
        }
        
        public func crop(picker:UIImagePickerController) -> UIImage? {
            guard picker.isViewLoaded && picker.view.window != nil else {
                print("Camera view is not ready.")
                return nil
            }
            
            UIGraphicsBeginImageContextWithOptions(picker.view.bounds.size, true, 0.0)
            defer { UIGraphicsEndImageContext() }
            guard let context = UIGraphicsGetCurrentContext() else { return nil }
            picker.view.layer.render(in: context)
            guard let fullImage = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else { return nil }
            let scaleFactor = CGFloat(fullImage.width) / picker.view.bounds.width
            let cropRect = CGRect(x: (outerFrameView.frame.origin.x+3) * scaleFactor,
                                  y: (outerFrameView.frame.origin.y+3) * scaleFactor,
                                  width: (outerFrameView.frame.width-6) * scaleFactor,
                                  height: (outerFrameView.frame.height-6) * scaleFactor)
            
            guard let croppedCGImage = fullImage.cropping(to: cropRect) else { return nil }
            let croppedImage = UIImage(cgImage: croppedCGImage)

            return croppedImage
        }

    }
