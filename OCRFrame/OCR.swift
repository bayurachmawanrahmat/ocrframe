//
//  OCR.swift
//  OCRFrame
//
//  Created by Ubayz on 04/07/24.
//

import Foundation
import UIKit

public struct OCR{
    
    public static func showVC(navigation:UINavigationController) -> UIViewController{
        let controller = OCRViewController()
        return controller
    }
    
    public static func sayHello(view:UIViewController){
        let common = Common()
        common.showAlert(view:view, title:"BAYU", message:"Camera Access Denied")
    }
}
