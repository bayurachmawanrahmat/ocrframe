Pod::Spec.new do |spec|
  spec.name           = "OCRFrame"
  spec.version        = "0.0.6"
  spec.summary        = "Custom frame for image picker activyty in iOS"
  spec.description    = "Custom frame for image picker activyty in iOS for OCR purpose"
  spec.homepage       = "https://gitlab.com/bayurachmawanrahmat/ocrframe.git"
  spec.license        = { :type => "MIT", :file => "LICENSE" }
  spec.author         = { "Bayu" => "bayurachmawanrahmat@gmail.com" }
  spec.source         = { :git => "https://gitlab.com/bayurachmawanrahmat/ocrframe.git", :tag => "#{spec.version}" }
  spec.platform       = :ios, '14.0'
  spec.source_files   = "OCRFrame", "OCRFrame/**/*.{h,swift}"
  spec.swift_version  = "5.0"
end



